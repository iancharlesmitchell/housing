module bitbucket.org/iancharlesmitchell/housing

go 1.15

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/joho/godotenv v1.3.0
)
