package housing

import (
	"fmt"
	"log"
	"os"
	"strconv"

	colly "github.com/gocolly/colly/v2"
	"github.com/joho/godotenv"
)

// Housing provides methods for data collection related to housing.
type Housing struct {
}

type Listing struct {
	Name        string
	ID          string
	URL         string
	Price       float32
	Description string
	RepostOf    string
}

// GetHousing initiates an instance of Housing.
func GetHousing() Housing {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	return Housing{}
}

func (h *Housing) generateCraigslistURLs(housingURL string, maxOffset int) []string {
	currentOffset := 0
	var urls []string
	for currentOffset < maxOffset {
		url := fmt.Sprintf("%s?s%d", housingURL, currentOffset)
		urls = append(urls, url)
		currentOffset += 120
	}
	return urls
}

func (h *Housing) getCraigslistListings(housingURL string, limit int) []Listing {
	c := colly.NewCollector(colly.Async())
	parallismFactor, err := strconv.Atoi(os.Getenv("PARALLELISM"))
	CheckError(err, "Error Getting Parallism Factor From ENV")
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: parallismFactor})
	log.Printf("Visiting: %s", housingURL)
	maxUrls := 3000
	listings := make(chan Listing, maxUrls)

	c.OnHTML(".result-row", func(e *colly.HTMLElement) {
		id := e.Attr("data-pid")
		repost := e.Attr("data-repost-of")
		url := e.ChildAttr("a", "href")
		listing := Listing{ID: id, RepostOf: repost, URL: url}
		listings <- listing
	})
	urls := h.generateCraigslistURLs(housingURL, maxUrls)
	for _, url := range urls {
		c.Visit(url)
	}
	c.Wait()
	close(listings)

	var processedListings []Listing
	for listing := range listings {
		processedListings = append(processedListings, listing)
	}
	return processedListings
}

func (h *Housing) CrawlCraigslist(housingURL string, limit int) {
	listings := h.getCraigslistListings(housingURL, limit)
	log.Println(len(listings))
}
