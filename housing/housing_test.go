package housing

import "testing"

func TestGenerateCraigslistURLs(t *testing.T) {
	housing := GetHousing()
	craigslistURL := "https://newyork.craigslist.org/d/apartments-housing-for-rent/search/apa"
	urlLimit := 3000
	urls := housing.generateCraigslistURLs(craigslistURL, urlLimit)
	if len(urls) != urlLimit/120 {
		t.Fatalf("Incorrect Number Of Urls Generated. Got: %d | Expected: %d", len(urls), urlLimit/120)
	}
}

func TestGetCraigslistListings(t *testing.T) {
	housing := GetHousing()
	craigslistURL := "https://newyork.craigslist.org/d/apartments-housing-for-rent/search/apa"
	urlLimit := 3000
	urls := housing.getCraigslistListings(craigslistURL, urlLimit)
	if len(urls) != urlLimit {
		t.Fatalf("Incorrect Number Of Urls Parsed. Got: %d | Expectd %d", len(urls), urlLimit)
	}
}

func TestCrawlCraigslist(t *testing.T) {
	housing := GetHousing()
	craigslistURL := "https://newyork.craigslist.org/d/apartments-housing-for-rent/search/apa"
	housing.CrawlCraigslist(craigslistURL, 0)
}
