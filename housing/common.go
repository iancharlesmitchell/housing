package housing

import "log"

// CheckError checks if an error is not null and logs if so.
func CheckError(err error, message string) {
	if err != nil {
		log.Println(message, err)
	}
}
